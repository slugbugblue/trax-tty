# trax-tty

This javascript module provides a simple to use interface for displaying a Trax
game on a tty console (ie, from nodejs).

Usage:

```javascript
import { Trax } from '@slugbugblue/trax'
import * as tty from '@slugbugblue/trax-tty'

const game = new Trax('trax', '@0/ A0+')
const players = ['first player', 'player 2']
tty.display(game, players)
```

See the [source](tty.js) for more details.
